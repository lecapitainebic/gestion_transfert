<?PHP 
require 'connect.php'; 
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.png" />

    <title>site de recruement des personel UASZ</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
   

    
     <link rel="stylesheet" href="css/style.css" rel="stylesheet">
     <link rel="stylesheet" href="css/Normalize.css" rel="stylesheet">
     <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

   

      <script src="js/jquery-1.11.3.min.js"></script>

        
  </head>
<body>

<?php include 'nav.php'; ?> 


<div class="container main" style="margin-top: 100px;">



<div class="row">

<?php 

  $stmt_count_produit = $connect->prepare("SELECT * FROM `clients`");
  $stmt_count_produit->execute();
  $count_produit= $stmt_count_produit->rowCount();

  $stmt_count_teachers = $connect->prepare("SELECT * FROM `traitement`");
  $stmt_count_teachers->execute();
  $count_teachers = $stmt_count_teachers->rowCount();



   ?>
<div class="col-md-12" id="status">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="info-box yellow-bg">
          <i class="fa fa-warehouse"><marquee><img src="image/uasz.jpg" width="150%"> <img src="image/uasz.jpg" alt="image" width="143%"></marquee></i>
            <div class="count"><?php echo $count_produit; ?></div>
            <div class="title">Le nombre de candidat</div>           
          </div>   
        </div>
        
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="info-box green-bg">
          <i class="fa fa-warehouse" ><marquee><img src="image/uasz.jpg" width="150%"> <img src="image/uasz.jpg " width="150%"></marquee></i>
            <div class="count"><?php echo $count_teachers; ?></div>
            <div class="title">Nombre de depot </div>            
          </div>    
        </div> 
        
       
</div>
 <div class="clear"></div><br>


    <div class="col-md-4">
      <a href="clients.php">
          <div class="link">
            <i class="fa fa-gg-circle"></i>
            <div class="clear"></div><span>Fait un depot</span>
         </div>
      </a>
    </div>
    
     <div class="col-md-4">
      <a href="consultation.php">
          <div class="link">
            <i class="fa fa-eur"></i>
            <div class="clear"></div><span>consultez votre compte</span>
         </div>
      </a>
    </div>
    
    <div class="col-md-4">
      <a href="traitement.php">
          <div class="link">
            <i class="fa fa-money"></i>
            <div class="clear"></div><span>Transaction</span>
         </div>
      </a>
    </div>
    <div class="col-md-4">
      <a href="clients!.php">
          <div class="link">
            <i class="fa fa-user"></i>
            <div class="clear"></div><span>client</span>
         </div>
      </a>
    </div>
    
     <div class="col-md-4">
      <a href="info_clients.php">
          <div class="link">
            <i class="fa fa-users"></i>
            <div class="clear"></div><span>Ajoutez clients</span>
         </div>
      </a>
    </div>
    
     <div class="col-md-4">
      <a href="reference_compte.php">
          <div class="link">
            <i class="fa fa-cog"></i>
            <div class="clear"></div><span>Operation</span>
         </div>
      </a>
    </div>

       

    

   


    

</div>
</div>		

<hr>

<div class="card-group">
  <div class="card">
    <img src="image/uasz1.jpg" class="card-img-top" alt="...">
    <div class="card-body">
      <h4 class="card-title">Avis de recrutement</h4>
      <p class="card-text">
1. Annonceur : Une entreprise de la place 
 Caissiers/caissières 
Description de l'offre :
Une entreprise de la place recrute des caissiers/caissières 
Formation : bac+2 en comptabilité compétence en gestion de la caisse ou en administration
Sens de l\'écoute et de travailler en équipe et sous tension
Date d'expiration : 22-08-2014
Dossier de candidature :
CV+lettre de motivation envoyer à enrecrut@gmail.com 

        .</p>
    </div>
    <div class="card-footer">
      <small class="text-muted">Service de transfert d'argent national </small>
    </div>
  </div>
  <div class="card">
    <img src="image/uasz1.jpg" class="card-img-top" alt="...">
    <div class="card-body">
      <h4 class="card-title">Avis de recrutement</h4>
      <p class="card-text">
      Le Centre régional des Œuvres universitaires de Ziguinchor lance des appels à candidatures pour les profils suivants :<br>

- Un Chef des Services administratifs.<b>Télécharger l'appel à candidatures.</b><br>
- Un Responsable financier et comptable.<b>Télécharger l'appel à candidatures.</b><br>
- Un Responsable des Ressources humaines.<b>Télécharger l'appel à candidatures.</b><br>
- Un Responsable Contrôle audit et qualité.<b>Télécharger l'appel à candidatures.</b><br>

La date limite de dépôt des dossiers de candidature est fixée au 05 mai 2022.
        </p>
    </div>
    <div class="card-footer">
      <small class="text-muted">Service de transfert international</small>
    </div>
  </div>
  <div class="card">
    <img src="image/uasz1.jpg" class="card-img-top" alt="...">
    <div class="card-body">
      <h4 class="card-title">Western-union</h4>
      <p class="card-text"> 
      Western Union Money Transfer (WUMT) est le leader du transfert d'argent international par lequel toute personne peut envoyer et/ou recevoir de l'argent à travers le monde. 
      Western a plus de 160 ans d'expériences et présent dans plus de 250 pays avec un réseau d'agence de plus de 550 000 points de vente.
      </p>
    </div>
    <div class="card-footer">
      <small class="text-muted" href="clients.php">Service de transfert international</small>
    </div>
  </div>
</div>



                           

<?php include 'footer.php'; ?>             

    <!-- Bootstrap & JavaScript
    ================================================== -->
    
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    
    <script src="js/ie10-viewport-bug-workaround.js"></script>



  </body>
</html>

