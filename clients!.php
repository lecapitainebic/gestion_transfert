<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.png" />

    <title>clients</title>

   <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
   

    
     <link rel="stylesheet" href="css/style.css" rel="stylesheet">
     <link rel="stylesheet" href="css/Normalize.css" rel="stylesheet">
     <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">


   

      <script src="js/jquery-1.11.3.min.js"></script>

    

     
        
  </head>
<body>


<?php 
require 'connect.php';
include 'nav.php'; ?> 




<div class="container mainbg">
<br><a class="return" href="index.php"><i class="glyphicon glyphicon-arrow-left"></i> Retour</a>

    <h1 class="h1_title">clients</h1>
    <hr> <br>

<?php 
if (isset($_POST['submit'])) {
   
  $clients=htmlspecialchars($_POST['clients']);
  $etat=htmlspecialchars($_POST['etat']);
  $date=htmlspecialchars($_POST['date']);
  $frais=htmlspecialchars($_POST['frais']);
 
  
  
  
  $ins_clients=$connect->prepare("INSERT INTO `traitement` (`id_clients`, `etat`, `frais`, `clients_id_clients`) VALUES (NULL, :etat, :date, :frais, :clients)");
  $ins_clients->bindParam(':clients' ,$clients, PDO::PARAM_STR);
  $ins_clients->bindParam(':etat' ,$etat, PDO::PARAM_STR);
  $ins_clients->bindParam(':date' ,$date, PDO::PARAM_STR);
  $ins_clients->bindParam(':frais' ,$frais , PDO::PARAM_STR);

  $ins_clients->execute();
  
 

  if (isset($ins_clients)) {
    echo "<div class='alert alert-success center' style='width: 90%; margin: auto;'><p>Ajout avec sucees</p></div><br><br>"; 
  }

  else {
   echo "<div class='alert alert-danger center' style='width: 90%; margin: auto;'><p>Error d'ajout</p></div><br><br>";     
  }

echo "<meta http-equiv='refresh' content='5; url = clients!.php' />";

 } 

?>

    <div class="clear"></div>
    <div class="row col-md-10 col-md-offset-1">

      <form id="formID" action="" method="post">
          
              <label class="">Nom du clients: <span style="color:red; font-weight: bold; font-family: Arial, sans-serif ;">(*)</span></label>
              <div class="input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                  <select name="clients" class="form-control validate[required]">
                  <option selected="selected" value="">Selectionnée</option>
<?php 
  $stmt_find_clients = $connect->query("SELECT * FROM `traitement`");

  while ($find_clients_row = $stmt_find_clients->fetch()) {
	  $fetch_clients_code =$find_clients_row['id_clients'];
      $fetch_clients_name = $find_clients_row ['nom_clients'];
	  $fetch_clients_prenom=$find_clients_row['prenom_clients'];

      echo '<option value="'.$fetch_clients_code.'">'.$fetch_clients_name.' '.$fetch_clients_prenom.'</option>';

  } 
?>
                  </select>
              </div><br>
               
     <label class="">etat_compte: <span style="color:red; font-weight: bold; font-family: Arial, sans-serif ;">(*)</span></label>
              <div class="input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-book"></i></span>
                  <textarea  class="form-control" name="etat_compte">
                  </textarea>
              </div><br>
    <label class="">Date de depot: <span style="color:red; font-weight: bold; font-family: Arial, sans-serif ;">(*)</span></label>
              <div class="input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                  <input name="date" type="date" placeholder="" class="form-control validate[required]" />
              </div><br>
       <label class="">Frais de service: <span style="color:red; font-weight: bold; font-family: Arial, sans-serif ;">(*)</span></label>
              <div class="input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-euro"></i></span>
                  <input name="frais" type="text" placeholder="" class="form-control validate[required]" />
              </div><br><br> 

          <button type="submit" name="submit" class="mybtn mybtn-success">Ajouter</button>     

          <hr id='success'>

      </form>
  
  </div>

<div class="clear"></div>
<?php 
if (isset($_GET['cat_delete']) ) {

$cat_id = $_GET['cat_delete'];

  $stmt_delete = $connect->prepare("DELETE FROM `categorie` WHERE `categorie`.`id_cat`=:id");
  $stmt_delete->bindParam (':id' , $cat_id , PDO::PARAM_STR );
  $stmt_delete->execute();

  if (isset($stmt_delete)) {
    echo "<div class='alert alert-success center' style='width: 90%; margin: auto;'><p>vous avez supprimé avec succés</p></div><br><br>"; 
    echo '<script type="text/javascript"> window.location.href += "#success"; </script>';
    echo "<meta http-equiv='refresh' content='5; url = categorie.php' />";
  }
  
}


 ?>
      <br>
        

</div>  
        
                           
           




  </body>
</html>
